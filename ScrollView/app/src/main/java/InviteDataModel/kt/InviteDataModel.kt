package InviteDataModel.kt

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class InviteRecords(
    @SerializedName("invite")
    @Expose
    var records: List<InviteDataModel>
)

class InviteDataModel {

        @SerializedName("title")
        @Expose
        var invitemodal_title: String? = null


        @SerializedName("description")
        @Expose
        var invitemodal_descri:String? = null


    @SerializedName("image")
    @Expose
    var modal_image: String? = null
}
