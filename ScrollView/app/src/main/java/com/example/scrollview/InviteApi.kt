package com.example.scrollview

import InviteDataModel.kt.InviteRecords
import io.reactivex.Observable
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET

interface InviteApi {
    @GET("home")
    fun getInvite():Observable<InviteRecords>
    companion object Factory{
        fun create():InviteApi{
            val retrofit = Retrofit.Builder()
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl("http://demo8588158.mockable.io/")
                .build()
            return retrofit.create(InviteApi::class.java)
        }
    }
}