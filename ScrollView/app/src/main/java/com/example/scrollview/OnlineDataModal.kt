package com.example.scrollview

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class OnlineDataModal {

    @SerializedName("descr")
    @Expose
    var modal_descr: String? = null

    @SerializedName("image")
    @Expose
    var modal_image: String? = null


}
