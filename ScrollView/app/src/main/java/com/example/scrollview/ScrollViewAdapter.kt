package com.example.scrollview


import InviteDataModel.kt.InviteDataModel
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.invitelayout_item.view.*


class ScrollViewAdapter(private val context: List<Any> = emptyList(), private val contextList:Context) :RecyclerView.Adapter<ScrollViewAdapter.BaseViewHolder<*>>() {


    companion object {
        private const val TYPE_INVITE = 0
        private const val TYPE_DETAILS = 1
        private const val TYPE_ONLINE = 2
    }

    abstract class BaseViewHolder<T>(itemView: View) : RecyclerView.ViewHolder(itemView) {
        abstract fun bind(item: T)
    }


    override fun onBindViewHolder(holder: BaseViewHolder<*>, position: Int) {
        val element = context[position]
        when (holder) {
            is InviteViewHolder -> holder.bind(element as InviteDataModel)

            is DetailViewHolder -> holder.bind(element as DetailsDataModal)

            is OnlineViewHolder -> holder.bind(element as OnlineDataModal)
        }
    }


  inner class InviteViewHolder(itemView: View) : BaseViewHolder<InviteDataModel>(itemView) {
     val titleInvite = itemView.inviteTitle_textView
      val decrInvite=itemView.inviteDec_textView


        override fun bind(element: InviteDataModel) {
            titleInvite.text = element.invitemodal_title
            decrInvite.text=element.invitemodal_descri
            Glide.with(itemView.context)
                .load(element.modal_image)
                .into(itemView.invite_imageView)
        }
    }

    inner class DetailViewHolder(itemView: View) : BaseViewHolder<DetailsDataModal>(itemView) {
        override fun bind(element: DetailsDataModal) {



        }
    }

    inner class OnlineViewHolder(itemView: View) : BaseViewHolder<OnlineDataModal>(itemView) {
        override fun bind(item: OnlineDataModal) {
        }
    }

    override fun getItemViewType(position: Int): Int {
        val comparable = context[position]
        return when (comparable) {
            is String -> TYPE_INVITE
            is String -> TYPE_DETAILS
            is String -> TYPE_ONLINE
            else -> throw IllegalArgumentException("Invalid type of data " + position)
        }
    }


    override fun getItemCount(): Int {
        return context.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder<*> {
        return when (viewType){
            TYPE_INVITE -> {
                val view = LayoutInflater.from(contextList)
                    .inflate(R.layout.invitelayout_item,parent,false)
                InviteViewHolder(view)
            }

            TYPE_DETAILS -> {
                val view = LayoutInflater.from(contextList)
                    .inflate(R.layout.detailslayout_item,parent,false)
                InviteViewHolder(view)
            }

            TYPE_ONLINE -> {
                val view = LayoutInflater.from(contextList)
                    .inflate(R.layout.detailslayout_item,parent,false)
                InviteViewHolder(view)
            }

            else -> throw IllegalArgumentException("Invalid view type")
        }
    }

}







