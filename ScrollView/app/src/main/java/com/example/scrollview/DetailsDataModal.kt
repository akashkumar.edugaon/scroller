package com.example.scrollview

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class DetailsDataModal {

    @SerializedName("expareDate")
    @Expose
    var modal_expareDate: String? = null

    @SerializedName("date")
    @Expose
    var modal_date: String? = null

    @SerializedName("service")
    @Expose
    var modal_service: String? = null

    @SerializedName("vehicle")
    @Expose
    var modal_vehicle: String? = null

    @SerializedName("bookingId")
    @Expose
    var modal_bookingId: String? = null

    @SerializedName("location")
    @Expose
    var modal_location: String? = null

    @SerializedName("image")
    @Expose
    var modal_image: String? = null
}
