package com.example.scrollview

import InviteDataModel.kt.InviteDataModel
import android.content.Context
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_invite_service.*


class InviteServiceFragment : Fragment() {

    companion object {
        fun newInstance() = InviteServiceFragment()
    }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_invite_service, container, false)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        callApi()
    }

fun callApi(){

    val APIService= InviteApi.create()
    APIService.getInvite()
        .observeOn(AndroidSchedulers.mainThread())
        .subscribeOn(Schedulers.io())
        .subscribe ({result->
            Log.i("result",result.toString())
            renderCountry(result.records)

        },{
                error->
            error.printStackTrace()
        })
}
fun renderCountry(inviteList: List<InviteDataModel>){
    val layoutManager=LinearLayoutManager(this.context,
    LinearLayoutManager.HORIZONTAL,false)
    invite_recyclerView.setLayoutManager(layoutManager)
    invite_recyclerView.adapter= activity?.let { ScrollViewAdapter(inviteList, it) }
}
}





